package hello;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

// Change the comment again to kick off a build
// This change is made originally on the ted2branch

public class TestGreeter {

   private Greeter g = new Greeter();

   @Test
   @DisplayName("Test for Empty Name")
   public void testGreeterEmpty()

   {
      assertEquals(g.getName(),"");
      assertEquals(g.sayHello(),"Hello!");
   }



   @Test
   @DisplayName("Test for Name='World'")
   public void testGreeter()
   {

      g.setName("World");
      assertEquals(g.getName(),"World");
      assertEquals(g.sayHello(),"Hello World!");
   }
   //
   //Test To make sure my name is correct
   //
   @Test
   @DisplayName("Test for Name='Francisco'")
   public void testGreeterFrancisco()
   {

      g.setName("Francisco");
      assertEquals(g.getName(),"Francisco");
      assertEquals(g.sayHello(),"Hello Francisco!");
   }
   //
   //Test to make sure it's not my nick name (Pancho)
   //
   @Test
   @DisplayName("Test for Name='Francisco , not my nickname'")
   public void testGreeterNotNickName()
   {

      g.setName("Francisco");
      assertFalse(g.getName() == "Pancho");
      assertFalse(g.sayHello() == "Hello Pancho!");
   }
   //
   //Test to make sure my lastname is right
   //
   @Test
   @DisplayName("Test for Name='Martell'")
   public void testGreeterLastName()
   {

      g.setName("Martell");
      assertEquals(g.getName(),"Martell");
      assertEquals(g.sayHello(),"Hello Martell!");
   }


   @Test
   @DisplayName("Test for Name='Daniel'")
   /**
    * Test to make sure my name is spelled correctly
    */
   public void testDanielGreeter() 
   {

      g.setName("Daniel");
      assertEquals(g.getName(),"Daniel");
      assertEquals(g.sayHello(),"Hello Daniel!");
   }


   @Test
   @DisplayName("Test for Name='Daniel' is never 'Dan'")
   /**
    * Test to make sure my name is never shortened
    */
   public void testDanielGreeterNeverDan() 
   {

      g.setName("Daniel");
      assertFalse(g.getName().equals("Dan"));
      assertFalse(g.sayHello().equals("Hello Dan!"));
   }

   @Test
   @DisplayName("Test for name='Logan'")
   /**
   * Test to make sure my name is displayed correctly
   */
   public void testLoganGreeter()
   {
      g.setName("Logan");
      assertEquals(g.getName(), "Logan");
      assertEquals(g.sayHello(), "Hello Logan!");
   }

   @Test
   @DisplayName("Test for Name='Logan' is never 'logan'")
   /**
   * Test to make sure my name is not automatically placed into lower case
   */
   public void testLoganGreeterlogan() {
      g.setName("Logan");
      assertFalse(g.getName().equals("logan"));
      assertFalse(g.sayHello().equals("Hello logan!"));
   }

}
