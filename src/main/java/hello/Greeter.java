package hello;

//This is Daniel's comment for A11, and now this too
//This is Logan's comment of A11
public class Greeter {

  private String name = "";


  public String getName() 

  {
    return name;
  }


  public void setName(String name) 

  {
      this.name = name;
  }


  public String sayHello() 

  {
  	if (name == "") 

    {
       return "Hello!";
    }
    else 
    {
       return "Hello " + name + "!";
    }

  }

}